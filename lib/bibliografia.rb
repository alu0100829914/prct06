require "prct06/version"

class Bibliografia
  
	attr_reader :autores, :titulo, :serie, :editorial, :n_editorial, :fecha, :isbn

	def initialize (v_autor, titulo, serie, editorial, n_editorial, fecha, v_isbn)

		@autores = v_autor
		@titulo = titulo

		if serie != 0

			@serie = serie

		end

		@editorial = editorial
		@n_editorial = n_editorial
		@fecha = fecha
		@isbn = v_isbn

	end


	def to_s

		"#{@autores} \n #{@titulo} \n #{@editorial}; #{@n_editorial} #{@fecha} \n #{@isbn}"

	end

end