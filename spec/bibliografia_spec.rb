require 'lib/bibliografia.rb'

describe Bibliografia do

	before :each do
		v_autor = ['autor1','autor2']
		serie = 0
		serie2 = 1
		v_isbn = ['ISBN-13: 978-1937785499', 'ISBN-10: 1937785491']
		fecha = '5 noviembre 2015'
		@biblio = Bibliografia.new(v_autor, 'titulo', serie, 'editorial', 6, fecha, v_isbn)
		@biblio2 = Bibliografia.new(v_autor, 'titulo', serie2, 'editorial', 6, fecha, v_isbn)
	end

	describe "Almacenamiento de variables" do
		## ------- AUTORES ------- ##
		it "Se almacena correctamente el autor" do
			expect(@biblio.autores[0]).to eq('autor1')
		end
		it "Se almacena correctamente un segundo autor" do
			expect(@biblio.autores[1]).to eq('autor2')
		end

		## ------- TITULO ------- ##
		it "Se almacena correctamente el titulo" do
			expect(@biblio.titulo).to eq('titulo')
		end

		## ------- SERIE ------- ##
		it "Se almacena correctamente la serie" do
			expect(@biblio.serie).to eq(nil)
		end

		it "Se almacena correctamente la serie" do
			expect(@biblio2.serie).to eq(1)
		end

		## ------- EDITORIAL ------- ##
		it "Se almacena correctamente la editorial" do
			expect(@biblio.editorial).to eq('editorial')
		end

		## ------- N_EDICION ------- ##
		it "Se almacena correctamente la numero de edicion" do
			expect(@biblio.n_editorial).to eq(6)
		end

		## ------- FECHA ------- ##
		it "Se almacena correctamente la numero de la fecha" do
			expect(@biblio.fecha).to eq('5 noviembre 2015')
		end

		## ------- NUMEROS ISBN ------- ##
		it "Se almacena correctamente el autor" do
			expect(@biblio.isbn[0]).to eq('ISBN-13: 978-1937785499')
		end
		it "Se almacena correctamente un segundo autor" do
			expect(@biblio.isbn[1]).to eq('ISBN-10: 1937785491')
		end
	end

	describe "Metodo to_s" do
		## ------- METODO TO_S ------- ##
		it "Comprobación del metodo to_s" do
			expect(@biblio.to_s).to eq("[\"autor1\", \"autor2\"] \n titulo \n editorial; 6 5 noviembre 2015 \n [\"ISBN-13: 978-1937785499\", \"ISBN-10: 1937785491\"]")
		end
	end
end